$(document).ready(function(){
	// Variabelen.
	var sterrenbeelden  = $('div.sterrenbeeld');
	var sterrenbeeld = $('div.sterrenbeeld p#sterrenbeeld');
	var afbeelding = $('div.sterrenbeeld p#afbeeldingSterrenbeeld');
	var dag = $('div#invoer input#dag');
	var maand = $('div#invoer input#maand');
	var dagok = false;
	var maandok = false;
		
	/* Geef een voorbeeld (placeholder) weer in de tekstvakken en
	   verwijder deze wanneer het tekstvak de focus krijgt.*/
	$('input:text, textarea').each(function(){
		var $this = $(this);   	
    	$this
    		.data('placeholder', $this.attr('placeholder'))
        	.focus(function(){$this.removeAttr('placeholder');})
        	.blur(function(){$this.attr('placeholder', $this.data('placeholder'));});
	});

	// Tooltips weergeven bij over het label van de tekstvakken gaan.
	$('label#dag')
      	.on('mouseenter', function showTooltip(){
        	$('<span class="tooltip">Type de dag van de maand in. In cijfers.</span>').appendTo('body');
      	})
	    .on('mouseleave', function hideTooltip(){
	      	$('span.tooltip').remove();
      	})
      	.on('mousemove', function changeTooltipPos(event){
        	var tooltipX = event.pageX -8;
        	var tooltipY = event.pageY +18;
        	$('span.tooltip').css({top: tooltipY, left: tooltipX});
    	});

    $('label#maand')
      	.on('mouseenter', function showTooltip(){
        	$('<span class="tooltip">Type de maand in. In cijfers.</span>').appendTo('body');
      	})
      	.on('mouseleave', function hideTooltip(){
        	$('span.tooltip').remove();
      	})
      	.on('mousemove', function changeTooltipPos(event){
        	var tooltipX = event.pageX -8;
        	var tooltipY = event.pageY +18;
        	$('span.tooltip').css({top: tooltipY, left: tooltipX});
    	});

    // Valideer de input in het tekstvak dag.
    dag.on('blur', function(){    	
    	if ($('#wrongDag'))
    	{
			$('#wrongDag').remove();
			$(this).removeClass('error');
		}		
		
		if (dag.val() !== "" && $.isNumeric(dag.val()))
		{
	        if (dag.val() < 1 || dag.val() > 31)
	        {
		        $(this)
					.addClass('error')
					.after('<span id="wrongDag">Voer een dag tussen 1 en 31 in.</span>');
				dagok = false;		
			}
			else
			{
				$(this).removeClass('error');
				dagok = true;
			}
	    }
		else if (dag.val() == "")
		{	  
	        $(this)
				.addClass('error')
				.after('<span id="wrongDag">Voer een dag in.</span>');
			dagok = false;
		}
		else if (dag.val() !== "" && !$.isNumeric(dag.val()))
		{		 
		    $(this)
				.addClass('error')
				.after('<span id="wrongDag">Voer een getal in.</span>');
			dagok = false;			
		}	    
	    else
	    {			
			$(this).removeClass('error');
		}
    });

    // Valideer de input in het tekstvak maand.
    maand.on('blur', function(){
    	if ($('#wrongMaand'))
    	{
			$('#wrongMaand').remove();
			$(this).removeClass('error');
		}

		if (maand.val() !== "" && $.isNumeric(maand.val()))
		{
			if (maand.val() < 1 || maand.val() > 12)
			{
		        $(this)
					.addClass('error')
					.after('<span id="wrongMaand">Voer een maand tussen 1 en 12 in.</span>');
				maandok = false;
		    }
		    else
		    {
		    	$(this).removeClass('error');
		    	maandok = true;
		    }
		}
		else if (maand.val() == "")
		{
	        $(this)
				.addClass('error')
				.after('<span id="wrongMaand">Voer een maand in.</span>');
			maandok = false;
	    }	    
	    else if (maand.val() !== "" && !$.isNumeric(maand.val()))
	    {
	    	$(this)
	    		.addClass('error')
	    		.after('<span id="wrongMaand">Voer een getal in.</span>');
	    	maandok = false;
	    }
	    else
	    {
			$(this).removeClass('error');
		}
    });

	// Toon het sterrenbeeld wanneer op de knop gedrukt wordt.
	$('input#bevestig').on('click', function(event){
		if (dagok === true && maandok === true)
		{			
			sterrenbeeld.empty();
			afbeelding.empty();

			if ((parseInt(dag.val()) >= 1 && parseInt(dag.val()) <= 19) && parseInt(maand.val()) == 1)
			{
				sterrenbeeld.text("Steenbok");
				afbeelding.prepend('<img src="images/steenbok.jpg" alt="Steenbok" title="Steenbok">');
			}
			else if ((parseInt(dag.val()) >= 20 && parseInt(dag.val()) <= 31) && parseInt(maand.val()) == 1)
			{
				sterrenbeeld.text("Waterman");
				afbeelding.prepend('<img src="images/waterman.jpg" alt="Waterman" title="Waterman">');
			}
			else if ((parseInt(dag.val()) >= 1 && parseInt(dag.val()) <= 18) && parseInt(maand.val()) == 2)
			{
				sterrenbeeld.text("Waterman");
				afbeelding.prepend('<img src="images/waterman.jpg" alt="Waterman" title="Waterman">');
			}
			else if ((parseInt(dag.val()) >= 19 && parseInt(dag.val()) <= 28) && parseInt(maand.val()) == 2)
			{
				sterrenbeeld.text("Vissen");
				afbeelding.prepend('<img src="images/vissen.jpg" alt="Vissen" title="Vissen">');
			}
			else if ((parseInt(dag.val()) >= 1 && parseInt(dag.val()) <= 20) && parseInt(maand.val()) == 3)
			{
				sterrenbeeld.text("Vissen");
				afbeelding.prepend('<img src="images/vissen.jpg" alt="Vissen" title="Vissen">');
			}
			else if ((parseInt(dag.val()) >= 21 && parseInt(dag.val()) <= 31) && parseInt(maand.val()) == 3)
			{
				sterrenbeeld.text("Ram");
				afbeelding.prepend('<img src="images/ram.jpg" alt="Ram" title="Ram">');
			}
			else if ((parseInt(dag.val()) >= 1 && parseInt(dag.val()) <= 20) && parseInt(maand.val()) == 4)
			{
				sterrenbeeld.text("Ram");
				afbeelding.prepend('<img src="images/ram.jpg" alt="Ram" title="Ram">');
			}
			else if ((parseInt(dag.val()) >= 21 && parseInt(dag.val()) <= 30) && parseInt(maand.val()) == 4)
			{
				sterrenbeeld.text("Stier");
				afbeelding.prepend('<img src="images/stier.jpg" alt="Stier" title="Stier">');
			}
			else if ((parseInt(dag.val()) >= 1 && parseInt(dag.val()) <= 21) && parseInt(maand.val()) == 5)
			{
				sterrenbeeld.text("Stier");
				afbeelding.prepend('<img src="images/stier.jpg" alt="Stier" title="Stier">');
			}
			else if ((parseInt(dag.val()) >= 22 && parseInt(dag.val()) <= 31) && parseInt(maand.val()) == 5)
			{
				sterrenbeeld.text("Tweelingen");
				afbeelding.prepend('<img src="images/tweeling.jpg" alt="Tweelingen" title="Tweelingen">');
			}
			else if ((parseInt(dag.val()) >= 1 && parseInt(dag.val()) <= 21) && parseInt(maand.val()) == 6)
			{
				sterrenbeeld.text("Tweelingen");
				afbeelding.prepend('<img src="images/tweeling.jpg" alt="Tweelingen" title="Tweelingen">');
			}
			else if ((parseInt(dag.val()) >= 22 && parseInt(dag.val()) <= 30) && parseInt(maand.val()) == 6)
			{
				sterrenbeeld.text("Kreeft");
				afbeelding.prepend('<img src="images/kreeft.jpg" alt="Kreeft" title="Kreeft">');
			}
			else if ((parseInt(dag.val()) >= 1 && parseInt(dag.val()) <= 22) && parseInt(maand.val()) == 7)
			{
				sterrenbeeld.text("Kreeft");
				afbeelding.prepend('<img src="images/kreeft.jpg" alt="Kreeft" title="Kreeft">');
			}
			else if ((parseInt(dag.val()) >= 23 && parseInt(dag.val()) <= 31) && parseInt(maand.val()) == 7)
			{
				sterrenbeeld.text("Leeuw");
				afbeelding.prepend('<img src="images/leeuw.jpg" alt="Leeuw" title="Leeuw">');
			}
			else if ((parseInt(dag.val()) >= 1 && parseInt(dag.val()) <= 22) && parseInt(maand.val()) == 8)
			{
				sterrenbeeld.text("Leeuw");
				afbeelding.prepend('<img src="images/leeuw.jpg" alt="Leeuw" title="Leeuw">');
			}
			else if ((parseInt(dag.val()) >= 23 && parseInt(dag.val()) <= 31) && parseInt(maand.val()) == 8)
			{
				sterrenbeeld.text("Maagd");
				afbeelding.prepend('<img src="images/maagd.jpg" alt="Maagd" title="Maagd">');
			}
			else if ((parseInt(dag.val()) >= 1 && parseInt(dag.val()) <= 22) && parseInt(maand.val()) == 9)
			{
				sterrenbeeld.text("Maagd");
				afbeelding.prepend('<img src="images/maagd.jpg" alt="Maagd" title="Maagd">');
			}
			else if ((parseInt(dag.val()) >= 23 && parseInt(dag.val()) <= 30) && parseInt(maand.val()) == 9)
			{
				sterrenbeeld.text("Weegschaal");
				afbeelding.prepend('<img src="images/weegschaal.jpg" alt="Weegschaal" title="Weegschaal">');
			}
			else if ((parseInt(dag.val()) >= 1 && parseInt(dag.val()) <= 22) && parseInt(maand.val()) == 10)
			{
				sterrenbeeld.text("Weegschaal");
				afbeelding.prepend('<img src="images/weegschaal.jpg" alt="Weegschaal" title="Weegschaal">');
			}
			else if ((parseInt(dag.val()) >= 23 && parseInt(dag.val()) <= 31) && parseInt(maand.val()) == 10)
			{
				sterrenbeeld.text("Schorpioen");
				afbeelding.prepend('<img src="images/schorpioen.jpg" alt="Schorpioen" title="Schorpioen">');
			}
			else if ((parseInt(dag.val()) >= 1 && parseInt(dag.val()) <= 22) && parseInt(maand.val()) == 11)
			{
				sterrenbeeld.text("Schorpioen");
				afbeelding.prepend('<img src="images/schorpioen.jpg" alt="Schorpioen" title="Schorpioen">');
			}
			else if ((parseInt(dag.val()) >= 23 && parseInt(dag.val()) <= 30) && parseInt(maand.val()) == 11)
			{
				sterrenbeeld.text("Boogschutter");
				afbeelding.prepend('<img src="images/boogschutter.jpg" alt="Boogschutter" title="Boogschutter">');
			}
			else if ((parseInt(dag.val()) >= 1 && parseInt(dag.val()) <= 20) && parseInt(maand.val()) == 12)
			{
				sterrenbeeld.text("Boogschutter");
				afbeelding.prepend('<img src="images/boogschutter.jpg" alt="Boogschutter" title="Boogschutter">');
			}
			else if ((parseInt(dag.val()) >= 21 && parseInt(dag.val()) <= 31) && parseInt(maand.val()) == 12)
			{
				sterrenbeeld.text("Steenbok");
				afbeelding.prepend('<img src="images/steenbok.jpg" alt="Steenbok" title="Steenbok">');
			}
			else
			{			
				sterrenbeeld.text("Geen correcte gegevens ingevoerd.");
				afbeelding.empty();				
			}

			classToShow = sterrenbeelden;			
			$(classToShow)
				.show(400);
			event.preventDefault();
		}
		else
		{			
			sterrenbeeld.text("Geen correcte gegevens ingevoerd.");
			afbeelding.empty();
			classToShow = sterrenbeelden;
			$(classToShow)
				.show(400);
			event.preventDefault();
		}
	});

	// Wanneer de focus gezet wordt op een tekstvak verdwijnt het sterrenbeeld weer.
	dag.add(maand).on('focus', function(event){
		classToHide = sterrenbeelden;
		$(this).val("");
		$(classToHide)
			.hide(400);
		event.preventDefault();	
	});	
});